'use strict';

var login = function() {
	
	var identifier = document.getElementById('input_identifier').value;
	var password = document.getElementById('input_password').value;
	
	restapiclient.login(identifier, password, function(err, req) {
		
		if(err) {
			console.error(err);
			return;
		}
		
		proApp.changeRoute('projects');
	});
};