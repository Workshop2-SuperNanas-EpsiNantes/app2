'use strict';

restapiclient.request.get(null, '/projects', function(err, req) {

    if(err) {
		console.log(err);
		return;
	}
	
	var html = '';
	
	for(var key in req.responseJSON) {
		
		var project = req.responseJSON[key];
		
		var now = new Date();
		var startDate = new Date(project.start_at);
		var endDate = new Date(project.end_at);
		
		html += '<a href="?route=" class="card">';
		
			html += '<span class="title">' + project.name + '</span>';
			html += '<span class="data"><span class="label">Date de début</span><span class="value">' +  startDate.toLocaleString() + '</span></span>';
			html += '<span class="data"><span class="label">Date de fin</span><span class="value">' + endDate.toLocaleString() + '</span></span>';
			html += '<span class="data"><span class="label">Nombre de crédit par groupe</span><span class="value">' + project.default_credits_count + '<span class="info">' + project.default_credits_count*10 + ' min</span>' + '</span></span>';
			html += '<span class="data"><span class="label">Nombre maximum de membres par groupes</span><span class="value">' + project.max_groups_members + '</span></span>';
			
			if(restapiclient.getUserType() === 1) {
				if(now.getTime() < startDate.getTime()) {
					html += '<button onclick="popup.error(\'Cette fonctionnalité sera prochainement disponible\'); return false;">Démarrer</button>';
				} else if(now.getTime() < endDate.getTime()) {
					html += '<button onclick="popup.error(\'Cette fonctionnalité sera prochainement disponible\'); return false;">Cloturer</button>';
				} else {
					html += '<button onclick="popup.error(\'Cette fonctionnalité sera prochainement disponible\'); return false;">Réouvrir</button>';
					html += '<button onclick="popup.error(\'Cette fonctionnalité sera prochainement disponible\'); return false;">Archiver</button>';
				}
				
				html += '<button onclick="popup.error(\'Cette fonctionnalité sera prochainement disponible\'); return false;">Éditer</button>';
			}
		
		html += '</a>';
	}
	
	document.getElementById('projects_list').innerHTML = html;
});