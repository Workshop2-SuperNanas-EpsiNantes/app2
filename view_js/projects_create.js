'use strict';

var projects_create = function() {
	
	var name = document.getElementById('input_name').value;
	var max_members = document.getElementById('input_max_members').value;
	var credits = document.getElementById('input_credits').value;
	var begin_date = document.getElementById('input_begin_date').value;
	var end_date = document.getElementById('input_end_date').value;
	
	restapiclient.request.post(null, '/projects/create', {name: name, max_members: max_members, credits: credits, begin_date: begin_date, end_date: end_date}, function(err, req) {
		
		if(err) {
			console.error(err);
			return;
		}
		
	});
};