'use strict';

restapiclient.request.get(null, '/users', function(err, req) {

    if(err) {
		console.log(err);
		return;
	}
	
	var html = '';
	
	for(var key in req.responseJSON) {
		
		var user = req.responseJSON[key];
		
		var typeString;
		switch(user.type) {
			case 1:
				typeString = 'Administrateur';
				break;
			case 2:
				typeString = 'Intervenant';
				break;
			case 3:
				typeString = 'Étudiant';
				break;
			default:
				typeString = '???';
		}
		
		html += '<a href="?route=" class="card">';
		
			html += '<span class="title">' + user.email + '</span>';
			html += '<span class="data"><span class="label">Nom d\'utilisateur</span><span class="value">' +  user.username + '</span></span>';
			html += '<span class="data"><span class="label">Type d\'utilisateur</span><span class="value">' + typeString + '</span></span>';
		
		html += '</a>';
	}
	
	document.getElementById('users_list').innerHTML = html;
});