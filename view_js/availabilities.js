'use strict';

restapiclient.request.get(null, '/availabilities', function(err, req) {

    if(err) {
		console.log(err);
		return;
	}
	
	var html = '';
	
	for(var key in req.responseJSON) {
		
		var project = req.responseJSON[key];
		
		var now = new Date();
		var startDate = new Date(project.available_from);
		var endDate = new Date(project.available_to);
		
		if(now.getTime() < endDate.getTime()) {
					
			html += '<a href="?route=" class="card">';
				
				html += '<span class="data"><span class="label">Intervenant</span><span class="value">' +  project.teacher_username + '</span></span><br />';
				html += '<span class="data"><span class="label">Date de début</span><span class="value">' +  startDate.toLocaleString() + '</span></span>';
				html += '<span class="data"><span class="label">Date de fin</span><span class="value">' + endDate.toLocaleString() + '</span></span>';
				
				html += '<br />';
				
				if(restapiclient.getUserType() === 2) {
					html += '<button onclick="popup.error(\'Cette fonctionnalité sera prochainement disponible\'); return false;">Reporter</button>';
				}
				if(restapiclient.getUserType() === 1 || restapiclient.getUserType() === 2) {
					html += '<button onclick="popup.error(\'Cette fonctionnalité sera prochainement disponible\'); return false;">Annuler</button>';
				}
			
			html += '</a>';
		}
	}
	
	document.getElementById('appointments_list').innerHTML = html;
});