'use strict';

var projects_create = function() {
	
	var date = document.getElementById('input_date').value;
	var begin_date = document.getElementById('input_begin_date').value;
	var end_date = document.getElementById('input_end_date').value;
	var teacher_id = document.getElementById('input_teacher').value;
	var message = document.getElementById('input_message').value;
	
	var start_at = date + ' ' + begin_date;
	var end_at = date + ' ' + end_date;
	
	restapiclient.request.post(null, '/groups/628924af-3d98-11e8-9458-acfdcee00f1e/create_appointement', {message: message, user_id: teacher_id, start_at: start_at, end_at: end_at}, function(err, req) {
		
		if(err) {
			console.error(err);
			return;
		}
		
	});
};

var init_projects_create = function() {

	var dateEl = document.getElementById('input_date');
	
	dateEl.setAttribute('min', '2018-04-12');
}