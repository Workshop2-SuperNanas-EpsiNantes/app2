'use strict';

restapiclient.request.get(null, '/groups', function(err, req) {

    if(err) {
		console.log(err);
		return;
	}
	
	var html = '';
	
	for(var key in req.responseJSON) {
		
		var group = req.responseJSON[key];
		
		html += '<a href="?route=" class="card">';
		
			html += '<span class="title">' + group.name + '</span>';
			html += '<span class="data"><span class="label">Projet</span><span class="value">' + group.project_name + '</span></span>';
			html += '<span class="data"><span class="label">Nombre de crédit restants</span><span class="value">' + group.credits_count + '<span class="info">' + group.credits_count*10 + ' min</span>' +  '</span></span>';
		
			if(restapiclient.getUserType() === 1) {
				html += '<br />';
				html += '<button onclick="popup.error(\'Cette fonctionnalité sera prochainement disponible\'); return false;">Modifier les crédits</button>';
				html += '<button onclick="popup.error(\'Cette fonctionnalité sera prochainement disponible\'); return false;">Supprimer</button>';
			}
		
		html += '</a>';
	}
	
	document.getElementById('groups_list').innerHTML = html;
});