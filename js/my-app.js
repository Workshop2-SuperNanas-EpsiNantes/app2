'use strict';

var myApp = function(routes, menu, initRouteId, defaultRouteId) {
	// ---------- Scope Privé ----------
	var mRoutes = {},
		mMenu = {},
		mXhrRouteViewReq = null,
		mRouteId = null;

	window.onpopstate = function(evt) {
		console.log(evt);

		var routeId = getUrlRouteId(false);

		if( routeId !== false && routeId !== mRouteId ) {
			changeRoute(routeId, false, true);
		}
	};

	function getUrlRouteId(defaultRouteId) {

		var routeId = utils.getUrlParam('route');

		if(!mRoutes[routeId]) {
			routeId = defaultRouteId;
		}

		return routeId;
	}

	function loadMenu( menu ) {

		mMenu = menu;

		var html = '';

		for(var i in menu) {
			var item = menu[i];

			html += '<span class="menuItem' + ((item.active == 'active')? ' active' : '') + '"' + ((item.action)? ' onclick="'+item.action+'"' : '') + ((item.route)? ' data-route="'+item.route+'"' : '') + ((item.category)? ' data-category="'+item.category+'"' : '') + ' >' +
				item.display +
				((item.label)? item.label : '' ) +
			'</span>';
		}

		document.getElementById('menu').innerHTML = html;
	}

	function selectMenuItem( itemCat ) {

		//var items = document.getElementById('menu').children;
		var items = document.getElementsByClassName('menuItem');

		for(var i = 0; i < items.length; i++) {
			if(items[i].getAttribute('data-category') == itemCat) {
				utils.addClass(items[i], 'active');
			}
			else {
				utils.removeClass(items[i], 'active');
			}
		}
	}
	
	function updateLinksWithHooks() {
		
		var items = document.getElementsByClassName('menuItem');
		
		for(var i = 0; i < items.length; i++) {
			
			var el = items[i];
			
			var targetRoute = el.getAttribute('data-route');
			var route = mRoutes[targetRoute];
			
			if(route && route.hooks) {
			
				el.style.display = null;
				for(var j in route.hooks) {
					
					if(route.hooks[j]() === false) {
						el.style.display = 'none';
						break;
					}
				}
			}
		}
	}

	function changeRoute( routeId, newWindow, isBack ) {

		if(mXhrRouteViewReq !== null) {
			mXhrRouteViewReq.abort();
		}

		var route = mRoutes[routeId];

		if(!route) {
			popup.error('La page n\'existe pas', 'La page n\'a pu être chargée');
			return;
		} else if(navigator.onLine === false && !route.offlineAccess) {
			var popupButtons = [
				{ 'value': 'OK' },
				{ 'value': 'Réessayer', 'action': 'proApp.changeRoute(\'' + routeId + '\');' }
			];
			popup.error('Vous n\'êtes pas connecté à Internet', 'La page n\'a pu être chargée', popupButtons);
			return;
		}

		if(route.url) { // Si une url est spécifiée, on change d'url et arrête le traitement
			window.location.href = route.url;
			return;
		}

		if(routeId === mRouteId) {
			setLoad(1);
			setTimeout(function() { setLoad(0); }, 200);
			return;
		}

		loadRoute(routeId, route, isBack);
	}

	function setLoad(coeff) {

		document.getElementById('content').className = 'load' + (Math.round(coeff*10)*10);
		document.getElementById('content').setAttribute('data-load', (coeff*100)+'%');
	}

	function loadRoute( routeId, route, isBack ) {

		var isAborted = false;
		var successDecrement = {};
		var total = 0;
		var data = {};

		setLoad(0.1);
		var slowTimeout = setTimeout(function() {
			utils.addClass(document.getElementById('content'), 'slow');
		}, 2000);

		/* * *
		*  Traitement des retours
		* * */
		var successCallback = function(type, xhrOrEvent) {
			if(isAborted) return;

			successDecrement[ type ]--;

			var reste = 0;
			for(var i in successDecrement) {
				reste = reste + successDecrement[i];
			}
			var fait = total-reste;
			setLoad(0.1 + fait/total*0.8);

			if(type === 'VIEW') {
				var req = xhrOrEvent;
				data[type] = req.responseText;
			}

			if(!reste) {
				clearTimeout(slowTimeout);
				applyRoute(routeId, route, data, isBack);
			}
		};
		var errorCallback = function(type, xhrOrEvent) {
			if(isAborted) return;
			isAborted = true;

			setLoad(0);
			clearTimeout(slowTimeout);
			utils.removeClass(document.getElementById('content'), 'slow');

			var popupButtons = [
				{ 'value': 'OK' },
				{ 'value': 'Réessayer', 'action': 'proApp.changeRoute(\'' + routeId + '\');' }
			];

			if(type === 'VIEW' ) {
				var req = xhrOrEvent;

				console.log(req);

				if(req.status === 0) {
					popup.error('Vérifiez votre connexion Internet', 'La page n\'a pu être chargée', popupButtons);
					console.log(req);
					return;
				} else if(req.status >= 500 && req.status < 600) {
					popup.error('Le serveur a rencontré une erreur', 'La page n\'a pu être chargée', popupButtons);
					return
				}
			}

			popup.error('Une erreur s\'est produite', 'La page n\'a pu être chargée', popupButtons);
		};

		/* * *
		*  Récupération des éléments de la route
		* * */
		var hasStyle = route.style && route.style.length;
		var hasScript = route.script && route.script.length;
		var hasView = route.view && true;

		if( hasStyle || hasScript || hasView ) {

			if(hasStyle) { // Si un ou des css sont spécifiés
				successDecrement['STYLE'] = route.style.length;
				for(var i in route.style) {
					total++;
					loadStyle(route.style[i], successCallback, errorCallback);
				}
			}
			if(hasScript) { // Si un ou des js sont spécifiés
				successDecrement['SCRIPT'] = route.script.length;
				for(var i in route.script) {
					total++;
					loadScript(route.script[i], successCallback, errorCallback);
				}
			}
			if(hasView) { // Si une vue est spécifiée
				successDecrement['VIEW'] = 1;
				total++;
				loadView(route.view, successCallback, errorCallback);
			}

		}
		else {
			clearTimeout(slowTimeout);
			applyRoute(routeId, route, data, isBack);
		}
	}

	function loadStyle( url, successCallback, errorCallback ) {

		var styleEl = document.createElement('link');

		styleEl.setAttribute('rel', 'stylesheet')
		styleEl.setAttribute('type', 'text/css')
		styleEl.setAttribute('href', url);

		styleEl.onload = function(e) {
			successCallback('STYLE', e);
		};
		styleEl.onerror = function(e) {
			errorCallback('STYLE', e);
		};

		document.body.appendChild(styleEl);
	}

	function loadScript( url, successCallback, errorCallback ) {

		var scriptEl = document.createElement('script');

		scriptEl.setAttribute('src', url);

		scriptEl.onload = function(e) {
			successCallback('SCRIPT', e);
		};
		scriptEl.onerror = function(e) {
			errorCallback('SCRIPT', e);
		};

		document.body.appendChild(scriptEl);
	}

	function loadView( url, successCallback, errorCallback ) {

		var req = new XMLHttpRequest();
		mXhrRouteViewReq = req;
	    req.open("GET", url);
		req.onreadystatechange = function (evt) {
			if (req.readyState == XMLHttpRequest.DONE) {// Requête terminée
				mXhrRouteViewReq = null;

				if(req.status >= 200 && req.status < 300) {// HTTP OK
					successCallback('VIEW', req);
				}
				else { // Si la requête n'a pas été terminée correctement
					errorCallback('VIEW', req)
				}
			}
		};
		req.send();
	}

	function applyRoute( routeId, route, data, isBack ) {

		utils.dispatchEvent(document, 'routeDeleted');

		setLoad(1);
		utils.removeClass(document.getElementById('content'), 'slow');

		document.getElementById('content').innerHTML = data['VIEW']? data['VIEW'] : '';

		utils.dispatchEvent(document, 'routeReady');

		if(typeof route.action === 'function') {
			route.action();
		}

		selectMenuItem(route.category);
		updateLinksWithHooks();
		document.title = route.title;

		if(!isBack && !route.sameUrl) {
			history.pushState(null, document.title, '?route='+routeId);
		}

		mRouteId = routeId;

		setTimeout(function() { setLoad(0); }, 200);
	}
	// ---------- !Scope Privé ----------


	// ---------- Init ----------
	mRoutes = routes;

	loadMenu(menu);

	if(!initRouteId && initRouteId !== false) {
		initRouteId = getUrlRouteId(defaultRouteId);
	}

	if(initRouteId) {
		changeRoute(initRouteId);
	}
	// ---------- !Init ----------


	// ---------- Scope Public ----------
	return {

		'getUrlRouteId': getUrlRouteId,

		'loadMenu': loadMenu,

		'changeRoute': changeRoute

	}
	// ---------- !Scope Public ----------
};