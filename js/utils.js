var utils = {

	/* ClassName*/
	containsClass: function(el, classname) {
		return el.classList.contains(classname);
	},
	addClass: function(el, classname) {
		el.classList.add(classname);
	},
	removeClass: function(el, classname) {
		el.classList.remove(classname);
	},
	toogleClass: function(el, classname) {
		el.classList.toogle(classname);
	},
	/* !ClassName */

	getUrlParam: function(key, def) {

		key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		var regex = new RegExp('[\\?&]' + key + '=([^&#]*)');
		var qs = regex.exec(window.location.href);
		if(qs == null) {
			return def;
		}
		else {
			return qs[1];
		}
	},

	dispatchEvent: function(target, eventName) {
		var event = document.createEvent('Event');
		event.initEvent(eventName, true, true);
		target.dispatchEvent(event);
	},

	playCssAnimationByClass(target, classname) {
		utils.removeClass(target, classname);
		void target.offsetWidth; // Reflow
		utils.addClass(target, classname);
	}
};


/* ClassName SHIM */
if(typeof document === "undefined" || !("classList" in document.createElement("a"))) {
	utils.containsClass = function(el, classname) {
		return el.className.split(' ').indexOf(classname) > -1;
	};
	utils.addClass = function(el, classname) {
		if(!utils.containsClass(el, classname)) el.className += ' ' + classname;
	};
	utils.removeClass = function(el, classname) {
		if(utils.containsClass(el, classname)) {
			var arrayClass = el.className.split(' ');
			arrayClass.splice(arrayClass.indexOf(classname), 1);
			el.className = arrayClass.join(' ');

			utils.removeClass(el, classname);
		}
	};
	utils.toogleClass = function(el, classname) {
		if(utils.containsClass(el, classname)) {
			utils.removeClass(el, classname);
		}
		else {
			utils.addClass(el, classname);
		}
	};
}
/* !ClassName SHIM */
