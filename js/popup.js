'use strict';

var popup = (function() {
	var popupContainer;
	var DEFAULT;

	// ---------- Scope Privé ----------
	function custom(message, title, buttons, drawable, cancelable, type) {

		var popupEl = document.createElement('div');
		popupEl.className = 'popup';
		popupEl.setAttribute('data-cancelable', cancelable);
		if(typeof type === 'string') popupEl.setAttribute('data-type', type);

		if(typeof message !== 'string') message = '';
		if(typeof title !== 'string') title = '';

		var popupHtml = '<div class="box">';
		popupHtml +=	 '<div class="content">';
		popupHtml +=		 '<div class="drawable">' + drawable + '</div>';
		popupHtml +=		 '<div class="title">' + title + '</div>';
		popupHtml +=		 '<div class="message">' + message + '</div>';
		popupHtml +=	 '</div>';

		popupHtml +=	 '<div class="buttons">';

		var actionTimeout = function(callback, seconds) {

			callback(seconds);

			if(seconds > 0) {
				setTimeout(function() {

					actionTimeout(callback, seconds-1);
				}, 1000);
			}
		};

		for(var i in buttons) {
			(function(i) {

				var backgroundColor = ' style="background-color:' + ((buttons[i].color)? buttons[i].color : ((buttons[i].action)? '#19AF9C' : '#0E0E0E' ) ) + '"';
				var action = ' onclick="(function(){' + ((buttons[i].action)? buttons[i].action : '' ) + 'popup.remove(this);}).call(this.parentNode.parentNode.parentNode);event.preventDefault();event.stopPropagation();"';

				popupHtml += '<div class="button"' + backgroundColor + action + '>' + buttons[i].value + '</div>';

				if(buttons[i].timeout) {

					actionTimeout(function(seconds) {
						console.log(seconds);
					}, buttons[i].timeout);
				}

			})(i);
		}

		popupHtml +=	 '</div>';
		popupHtml += '</div>';

		popupEl.innerHTML = popupHtml;

		popupContainer.appendChild(popupEl);

		setTimeout(function() {
			if(cancelable) {
				popupEl.onclick = remove.bind(this, popupEl);
			} else {
				popupEl.onclick = function() {
					utils.playCssAnimationByClass(popupEl, 'popupShake');
				};
			}
		}, 500);

		utils.playCssAnimationByClass(popupEl, 'in');

		return popupEl;
	}

	function ask(message, title, buttons, drawable) {

		if(typeof message === 'undefined' || message === DEFAULT) message = '?';
		if(typeof title === 'undefined' || title === DEFAULT) title = '';
		if(typeof drawable === 'undefined' || drawable === DEFAULT) drawable = '<i class="icon-menun"></i>';
		if(typeof buttons === 'undefined' || buttons === DEFAULT) buttons = [ { 'value': 'OK', 'action': '' }  ];

		return custom(message, title, buttons, drawable, false, 'ask');
	}

	function wait(message, title, buttons, drawable) {

		if(typeof message === 'undefined)' || message === DEFAULT) message = 'Veuillez patienter...';
		if(typeof title === 'undefined' || title === DEFAULT) title = '';
		if(typeof drawable === 'undefined' || drawable === DEFAULT) drawable = '<i class="icon-wait"></i>';
		if(typeof buttons === 'undefined' || buttons === DEFAULT) buttons = [];

		return custom(message, title, buttons, drawable, false, 'wait');
	}

	function success(message, title, buttons, drawable) {

		if(typeof message === 'undefined' || message === DEFAULT) message = 'L\'opération a réussie';
		if(typeof title === 'undefined' || title === DEFAULT) title = '';
		if(typeof drawable === 'undefined' || drawable === DEFAULT) drawable = '<i class="icon-success"></i>';
		if(typeof buttons === 'undefined' || buttons === DEFAULT) buttons = [ { 'value': 'OK', 'action':'' } ];

		return custom(message, title, buttons, drawable, true, 'success');
	}

	function error(message, title, buttons, drawable) {

		if(typeof message === 'undefined' || message === DEFAULT) message = 'L\'opération a échouée';
		if(typeof title === 'undefined' || title === DEFAULT) title = '';
		if(typeof drawable === 'undefined' || drawable === DEFAULT) drawable = '<i class="icon-error"></i>';
		if(typeof buttons === 'undefined' || buttons === DEFAULT) buttons = [ { 'value': 'OK', 'action': '' } ];

		return custom(message, title, buttons, drawable, true, 'error');
	}

	function fatal(message, title, buttons, drawable) {

		removeAll();

		if(typeof message === 'undefined' || message === DEFAULT) message = 'Une erreur s\'est produite...';
		if(typeof title === 'undefined' || title === DEFAULT) title = '';
		if(typeof drawable === 'undefined' || drawable === DEFAULT) drawable = '<i class="icon-fatal"></i>';
		if(typeof buttons === 'undefined' || buttons === DEFAULT) buttons = [ { 'value': 'Accueil', 'color': '#0E0E0E', 'action': 'window.location.href=\'/\';return;' }, { 'value': 'Recharger', 'action': 'window.location.reload();return;' } ];

		return custom(message, title, buttons, drawable, false, 'fatal');
	}

	function remove(popupEl) {

		utils.removeClass(popupEl, 'in');

		setTimeout(function() {
			if(popupEl) {
				popupEl.parentNode.removeChild(popupEl);
				popupEl = null;
			}
		}, 500);
	}

	function removeType(type) {

		var popupEls =  popupContainer.children;
		for(var i = 0, iend = popupEls.length; i < iend; i++) {
			if(popupEls[i].getAttribute('data-type') === type) {
				remove(popupEls[i]);
			}
		}
	}

	function removeAll() {

		var popupEls =  popupContainer.children;
		for(var i = 0, iend = popupEls.length; i < iend; i++) {
			remove(popupEls[i]);
		}
	}
	// ---------- !Scope Privé ----------

	// ---------- Init ----------

	popupContainer = document.createElement('div');
	popupContainer.id = 'popupContainer';
	document.body.appendChild(popupContainer);

	DEFAULT = 'DEFAULT';

	// ---------- !Init ----------

	// ---------- Scope Public ----------
	return {

		'custom': custom,

		'ask': ask,

		'wait': wait,

		'success': success,

		'error': error,

		'fatal': fatal,

		'remove': remove,

		'removeType': removeType,

		'removeAll': removeAll,

		'DEFAULT': DEFAULT

	}
	// ---------- !Scope Public ----------
})();