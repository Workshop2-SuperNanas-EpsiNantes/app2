'use strict';
	
var httpRequest = {
	
	request: function(method, url, data, options, callback) {
		
		var httpRequest = new XMLHttpRequest();
		httpRequest.open(method, url, true);
		
		if(options && options.headers) {
			for(var key in options.headers) {
				
				var header = options.headers[key];
				
				httpRequest.setRequestHeader(header[0], header[1]);
			}
		}
		
		if(data !== null) {
			data = JSON.stringify(data);
            httpRequest.setRequestHeader('Content-Type', 'application/json');
		}
		
		httpRequest.onreadystatechange = function(event) {
			
			if(httpRequest.readyState !== XMLHttpRequest.DONE) {
				return;
			}
			
			if(typeof callback !== 'function') {
				return;
			}
			
			if(httpRequest.status != 204) {
				var responseContentType = httpRequest.getResponseHeader('content-type');
				if(responseContentType && responseContentType.indexOf('application/json') > -1) {
					
					try {
						httpRequest.responseJSON = JSON.parse(httpRequest.responseText);
					} catch(e) {
						
						callback({
							errorCode: -1,
							errorMessage: 'JSON Format error'
						}, httpRequest);
						return;
					}
				}
			}
			
			if(httpRequest.status < 200 || httpRequest.status > 299) {
				callback({
					errorCode: httpRequest.status,
					errorMessage: 'undefined error'
					}, httpRequest );
				return;
			}
			
			callback(null, httpRequest);
		};
		
		httpRequest.send(data);
	},
	get: function(url, options, callback) {
		
		return httpRequest.request('GET', url, null, options, callback);
	},
	post: function(url, data, options, callback) {
		
		return httpRequest.request('POST', url, data, options, callback);
	},
	put: function(url, data, options, callback) {
		
		return httpRequest.request('PUT', url, data, options, callback);
	},
	delete: function(url, data, options, callback) {
		
		return httpRequest.request('DELETE', url, data, options, callback);
	}
};