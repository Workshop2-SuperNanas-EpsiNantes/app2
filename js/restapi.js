'use strict';

var RestapiClient = function(apiUrl, restOptions) {

	// PARAMS
	var linksKey = (restOptions && typeof restOptions.linksKey === 'string'? restOptions.linksKey : 'links');

	// DATA
	var sessionToken = null;
	var baseLinks = null;
	var authLinks = null;
	
	var userType = -1;
	
	// SESSION TOKEN
	var setSessionToken = function(st) {
		
		sessionToken = st;
		
		if(sessionToken === null) {
			
			localStorage.removeItem('rest_sessionToken');
			
			userType = -1;
			
			if(restOptions && typeof restOptions.onrestdisconnected === 'function') {
				restOptions.onrestdisconnected();
			}
			return;
		}
		
		localStorage.setItem('rest_sessionToken', st);
		
		var payload = JSON.parse(atob(sessionToken.split('.')[1]));
		userType = payload.userType;
	};
	
	// INIT LINKS
	var initBaseLinks = function(callback) {
			
		httpRequest.get(apiUrl, null, function(err, req) {
			
			if(err || !req.responseJSON || !req.responseJSON[linksKey]) {
				
				popup.error('La connexion au service à rencontrée un problème');
				if(typeof callback === 'function') callback(true);
				return;
			}
			
			baseLinks = req.responseJSON[linksKey];
			callback(false);
		});
	};
	
	// REST REQUESTS
	var restRequestModule = function(method, obj, link, data, options, callback) {
		
		var url = (obj === null? apiUrl + link : apiUrl + obj[link]);
		
		httpRequest.request(method, url, data, options, callback);
	};
	
	// BASE REST REQUESTS
	var baseRequestModule = function(method, link, data, options, callback) {
		
		var gotBaseLinks = function(err) {
			
			restRequestModule(method, baseLinks, link, data, options, callback);
		};
		
		if(baseLinks === null) {
			initBaseLinks(gotBaseLinks);
			return;
		}
		
		gotBaseLinks(false);
	};
	
	// LOGIN
	var login = function(identifier, password, callback) {
		
		var payload = {
			identifier: identifier,
			password: password
		};
		
		var loginCallback = function(err, req) {
			
			if(!err) {
				setSessionToken(req.responseJSON.sessionToken);
			}
			
			if(typeof callback === 'function') callback(err, req);
		};
		
		baseRequestModule('POST', 'login', payload, null, loginCallback);
	};
	
	var logout = function() {
		setSessionToken(null);
	};
	
	// AUTH REST REQUESTS
	var authRequestsModule = {
		request: function(method, obj, link, data, callback) {
			
			var authCallback = function(err, req) {
				
				if(err && err.errorCode === 401) {
					setSessionToken(null);
				}
				
				callback(err, req);
			};
			
			if(!sessionToken) {
				authCallback({errorCode: 401, errorMessage: 'not logged'}, null);
				return;
			}
			
			var options = {
				headers: [
					['Authorization', 'JWT ' + sessionToken]
				]
			};
			
			restRequestModule(method, obj, link, data, options, authCallback);
		},
		get: function(obj, link, callback) {
			authRequestsModule.request('GET', obj, link, null, callback);
		},
		post: function(obj, link, data, callback) {
			authRequestsModule.request('POST', obj, link, data, callback);
		},
		put: function(obj, link, data, callback) {
			authRequestsModule.request('PUT', obj, link, data, callback);
		},
		delete: function(obj, link, data, callback) {
			authRequestsModule.request('DELETE', obj, link, data, callback);
		}
	};
	
	setSessionToken(localStorage.getItem('rest_sessionToken'));
	
	return {
		login: login,
		logout: logout,
		request: authRequestsModule,
		getUserType: function() {
			return userType;
		}
	};
};